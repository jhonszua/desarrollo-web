postt = document.getElementById('post');

document.addEventListener('DOMContentLoaded', mostrarpelis);

botonMenu = document.getElementById("botonM");

botonMenu.addEventListener('click', menu);



function menu(event) {
    event.preventDefault();
    x = document.getElementById('navbarNavDropdown');
    if (x.style.display === 'none') {
        x.style.display = 'block';
    } else {
        x.style.display = 'none';
    }


}

function mostrarpelis() {
    let html = " ";

    const xhr = new XMLHttpRequest();
    xhr.open('GET', 'list.json', true);

    xhr.onload = function() {

        if (this.status == 200) {

            const seris = JSON.parse(this.responseText);

            seris.forEach(function(seri) {
                html += `

              
            
    <div class="card" class="jumbotron" style="width: 18rem;">
    <img src="${seri.imagen}" class="card-img-top" id="img-post">
    <div class="card-body justify-content-between">

        <h5 class="card-title">${seri.nombre}</h5>

        <p class="card-text">${seri.descripcion} </p>
    </div>
    <a href="#" class="btn btn-primary w-100">Trailer </a></div>

`;
            });


        }
        postt.innerHTML = html;
    }
    xhr.send();
}