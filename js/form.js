const nombre = document.getElementById('nombre');
const email = document.getElementById('email');
const tel = document.getElementById('tel');
const mensaje = document.getElementById('mensaje');
const enviar = document.getElementById('enviar-correo');
const form = document.querySelector('#formulario');
nombre.classList.remove('input-correct', 'input-error');
email.classList.remove('input-correct', 'input-error');
tel.classList.remove('input-correct', 'input-error');
mensaje.classList.remove('input-correct', 'input-error');
const numeros = "0123456789";
var letras = "abcdefghyjklmnñopqrstuvwxyz";

// nosotros-html


// eventos
eventos();


function eventos() {
    // Formulario inicio
    document.addEventListener('DOMContentLoaded', inicio);
    nombre.addEventListener('blur', validar);
    email.addEventListener('blur', validar);
    tel.addEventListener('blur', validar);
    mensaje.addEventListener('blur', validar);
    enviar.addEventListener('click', enviarEmail)



}

// Funciones
// funciones de formulario

function inicio() {
    enviar.disabled = true;
    enviar.style.backgroundColor = '#dbdbdb';

    enviar.classList.remove('disableB', 'habiliB');
    enviar.classList.add('disableB');



}

function enviarEmail(event) {

    const spinnerG = document.querySelector('#spinner');
    spinnerG.style.display = 'block';

    const enviado = document.createElement('img');
    enviado.src = 'img/mail.gif';
    enviado.width = '50';
    enviado.style.display = 'block';
    // tiempo
    setTimeout(function() {
        spinnerG.style.display = 'none';
        document.querySelector('#loaders').appendChild(enviado);
        setTimeout(function() {
            enviado.remove();
            form.reset();
            nombre.classList.remove('input-correct', 'input-error');
            email.classList.remove('input-correct', 'input-error');
            tel.classList.remove('input-correct', 'input-error');
            mensaje.classList.remove('input-correct', 'input-error');

            enviar.disabled = true;
            enviar.style.backgroundColor = '#dbdbdb';

            enviar.classList.remove('disableB', 'habiliB');
            enviar.classList.add('disableB');
        }, 2000);

    }, 3000);

    event.preventDefault();
}

function validar() {
    validarlonguitud(this);


    switch (this.type) {

        case 'text':
            if (this.id == 'nombre') {
                validarNombre(this);
            }
            break;
        case 'email':
            validarEmail(this);
            break;
        case 'tel':
            validarTel(this);
            break;


    }

    let errores = document.querySelectorAll('.input-error');

    if (nombre.value !== '' && email.value !== '' && tel.value !== '' && mensaje.value !== '') {

        if (errores.length == 0) {
            enviar.disabled = false;
            enviar.style.backgroundColor = '#000';
            enviar.classList.remove('disableB', 'habiliB');
            enviar.classList.add('habiliB');
            console.log('completo');
        }

    }
    if (errores.length > 0) {
        enviar.disabled = true;
        enviar.style.backgroundColor = '#dbdbdb';

        enviar.classList.remove('disableB', 'habiliB');
        enviar.classList.add('disableB');
    }
}

function validarlonguitud(campo) {


    if (campo.value.length > 0) {

        campo.classList.add('input-correct');
        campo.classList.remove('input-error');
    } else {
        campo.classList.add('input-error');

        campo.classList.remove('input-correct');

    }

}

function validarEmail(campo) {
    const mensa = campo.value;
    if (mensa.indexOf('@') !== -1 && mensa.indexOf('.') !== -1) {
        campo.classList.add('input-correct');
        campo.classList.remove('input-error');
    } else {
        campo.classList.add('input-error');
        campo.classList.remove('input-correct');

    }
}

function validarNombre(campo) {

    const mensa = campo.value;
    let tiene = tiene_numero(mensa);


    if (tiene == 1) {

        campo.classList.add('input-error');
        campo.classList.remove('input-correct');
        alert('El nombre no debe contener numeros');
    }

}

function tiene_numero(text) {
    for (i = 0; i < text.length; i++) {
        if (numeros.indexOf(text.charAt(i), 0) != -1) {
            return 1;
        }
    }
    return 0;
}

function tiene_letras(text) {
    text = text.toLowerCase();
    for (i = 0; i < text.length; i++) {
        if (letras.indexOf(text.charAt(i), 0) != -1) {
            return 1;
        }
    }
    return 0;
}

function validarTel(campo) {

    const mensa = campo.value;
    let tiene = tiene_letras(mensa);


    if (tiene == 1) {
        campo.classList.add('input-error');
        campo.classList.remove('input-correct');
        alert('El telefono no deba contener letras');
    }
}